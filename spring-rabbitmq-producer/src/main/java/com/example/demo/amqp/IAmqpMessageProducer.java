package com.example.demo.amqp;

public interface IAmqpMessageProducer<T> {
    void sendMessageProducer(T t);
}
