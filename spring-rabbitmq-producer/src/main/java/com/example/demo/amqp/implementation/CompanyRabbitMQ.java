package com.example.demo.amqp.implementation;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.example.demo.amqp.IAmqpMessageProducer;
import com.example.demo.dto.CompanyDto;

@Component
public class CompanyRabbitMQ implements IAmqpMessageProducer<CompanyDto> {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Value("${spring.rabbitmq.request.routing-key.company-producer}")
	private String queue;

	@Value("${spring.rabbitmq.request.exchenge.company-producer}")
	private String exchange;

	@Override
	public void sendMessageProducer(CompanyDto companyDto) {
		try {
			rabbitTemplate.convertAndSend(exchange, queue, companyDto);
		} catch (Exception ex) {
			throw new AmqpRejectAndDontRequeueException(ex);
		}
	}

}
