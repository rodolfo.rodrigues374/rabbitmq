package com.example.demo.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CompanyProducerRabbitMQConfiguration {
	  
		@Value("${spring.rabbitmq.request.routing-key.company-producer}")
	    private String queueConpany;

	    @Value("${spring.rabbitmq.request.exchenge.company-producer}")
	    private String exchangeConpany;

	    @Value("${spring.rabbitmq.request.routing-key-failure.company-producer}")
	    private String deadLetterConpany;

	    @Value("${spring.rabbitmq.request.routing-key-parking-lost.company-producer}")
	    private String parkingLotConpany;

	    @Bean
	    DirectExchange exchangeConpany() {
	        return new DirectExchange(exchangeConpany);
	    }

	    @Bean
	    Queue deadLetterConpany() {
	        return QueueBuilder.durable(deadLetterConpany)
	                .deadLetterExchange(exchangeConpany)
	                .deadLetterRoutingKey(queueConpany)
	                .build();
	    }

	    @Bean
	    Queue queueConpany() {
	        return  QueueBuilder.durable(queueConpany)
	                .deadLetterExchange(exchangeConpany)
	                .deadLetterRoutingKey(deadLetterConpany)
	                .build();
	    }

	    @Bean
	    Queue parkingLotConpany() {
	        return new Queue(parkingLotConpany);
	    }

	    @Bean
	    public Binding bindingQueueConpany() {
	        return BindingBuilder.bind(queueConpany())
	                .to(exchangeConpany()).with(queueConpany);
	    }

	    @Bean
	    public Binding bindingDeadLetterConpany() {
	        return BindingBuilder.bind(deadLetterConpany())
	                .to(exchangeConpany()).with(deadLetterConpany);
	    }

	    @Bean
	    public Binding bindingParkingLotConpany() {
	        return BindingBuilder.bind(parkingLotConpany()).to(exchangeConpany()).with(parkingLotConpany);
	    }

}
