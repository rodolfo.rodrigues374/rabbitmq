package com.example.demo.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.amqp.IAmqpMessageProducer;
import com.example.demo.dto.CompanyDto;
import com.example.demo.service.ICompany;

@Service
public class CompanyService implements ICompany {

	@Autowired
    private IAmqpMessageProducer<CompanyDto> amqpProducer;
	
	@Override
	public void sendMessageCompanyToQueue(CompanyDto companyDto) {
					
		this.amqpProducer.sendMessageProducer(companyDto);	 
	}

}
