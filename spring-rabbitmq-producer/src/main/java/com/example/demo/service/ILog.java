package com.example.demo.service;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.dto.MensagemErroDto;

public interface ILog {
	 MensagemErroDto log(final Throwable cause, final HttpServletRequest req);
}
