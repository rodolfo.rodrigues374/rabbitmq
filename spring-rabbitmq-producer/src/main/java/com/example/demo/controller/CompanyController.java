package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CompanyDto;
import com.example.demo.dto.MensagemErroDto;
import com.example.demo.service.ICompany;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/company")
@SuppressWarnings("rawtypes")
@Tag(name = "Company", description = "Company Producer Message API")
public class CompanyController extends BaseResource {

	@Autowired
	private ICompany companyService;

	@Operation(summary = "Send message company", description = "this operation sends the object Company record to a queue at Rabbitmq.", tags = {
			"company" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Compay object sent with to queue successfully.", content = @Content(array = @ArraySchema(schema = @Schema(implementation = CompanyDto.class)))) })
	@RequestMapping(value = "/send-message-company-to-queue", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity sendToConsumer(@Valid @RequestBody CompanyDto companyDto, HttpServletRequest req) {

		try {
			companyService.sendMessageCompanyToQueue(companyDto);
			return new ResponseEntity<Void>(HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
