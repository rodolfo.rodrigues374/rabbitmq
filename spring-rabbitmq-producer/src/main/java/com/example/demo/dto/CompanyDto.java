package com.example.demo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@ToString
public class CompanyDto {

	@NotNull(message = "Code is mandatory.")
	private Integer code;
	
	@NotNull(message = "Number Company is mandatory.")
	private Integer numberCompany;
	
	@NotBlank(message = "Name Company is mandatory")
	private String nameCompany;
}
