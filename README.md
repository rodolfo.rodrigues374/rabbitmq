# Implementation of message sending between two applications with Rabbitmq

I created two projects to simulate the sending of messages between two Java applications.

The producer application (spring-rabbitmq-producer) sends a json object to a queue at Rabbitmq.
Rabbitmq receives the message sent by the producer through the settings of Exchange Direct, the exchange sends the message to the router key that is waiting for some service to consume that message.
The consumer application (spring-rabbitmq-consumer) reads messages in Rabbitmq by configuring a listener pointing to the router key.

In this simulation, an exchange and three queues were created, following the description of each one.

| Exchange | Description |
| ------ | ------ |
|company.ex| Exchange Company - This is an Exchange of type Direct, which sends messages looking for a binding key equal to the routing key provided |
 
| Queue | Description |
| ------ | ------ |
| company.rk  | Routing Key - The name of the destination queue to send messages to another application |
| company.rkf | Routing key Failure - This queue is used to send messages that had a processing error in the company.rk queue. To simulate the error, just send the company object by filling in the value 50 in the code property.|
| company.rkpl  | Routing Key Parking Lost - This queue receives messages after three error processing attempts by company.rkf. The messages in the company.rkpl queue await further analysis. |

  
## Execution Rabbitmq

Use the docker to start a rabbit on the spot

```sh
docker run -d -p 15672:15672 -p 5672:5672 --name rabbitmq rabbitmq:3-management
```

Link to access the Rabbtmq service.
http://localhost:15672/

![header][image-rabbitmq-url]

Rabbitmq authentication

```sh
username: guest
senha: guest
```

To move messages, the shovel plugin must be enabled.

Access the bash of the container
```sh
docker exec -it rabbitmq bash
```

To enable shovel plugin execute the command below
```sh
rabbitmq-plugins enable rabbitmq_shovel rabbitmq_shovel_management
```

## Execution Producer

Run the local producer application and access the Swagger link to send a message to rabbitmq.

http://localhost:8081/rabbitmq-producer/swagger-ui.html

![header][image-swagger-producer-url]

## Execution Consumer

Run the local consumer application and access the Swagger link to receive a message to rabbitmq.

http://localhost:8082/rabbitmq-consumer/swagger-ui.html

![header][image-swagger-consumer-url]


 

[image-swagger-producer-url]: https://gitlab.com/rodolfo.rodrigues374/rabbitmq/-/raw/master/img/swagger-producer.png

[image-swagger-consumer-url]: https://gitlab.com/rodolfo.rodrigues374/rabbitmq/-/raw/master/img/swagger-consumer.png

[image-rabbitmq-url]: https://gitlab.com/rodolfo.rodrigues374/rabbitmq/-/raw/master/img/rabbitmq.png




