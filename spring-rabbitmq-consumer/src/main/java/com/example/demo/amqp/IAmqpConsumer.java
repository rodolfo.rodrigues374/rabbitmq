package com.example.demo.amqp;

public interface IAmqpConsumer<T> {
    void consumer(T t);
}
