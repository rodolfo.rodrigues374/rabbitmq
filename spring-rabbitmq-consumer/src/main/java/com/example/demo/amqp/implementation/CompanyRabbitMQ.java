package com.example.demo.amqp.implementation;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.amqp.IAmqpConsumer;
import com.example.demo.dto.CompanyDto;
import com.example.demo.service.implementation.CompanyService;

@Component
public class CompanyRabbitMQ implements IAmqpConsumer<CompanyDto> {

    @Autowired
    private CompanyService companyService;

    @Override
    @RabbitListener(queues = "${spring.rabbitmq.request.routing-key.company-consumer}")
    public void consumer(CompanyDto companyDto) {
        try {
        	companyService.receiveMessageComsumer(companyDto);
        } catch (Exception ex) {
            throw new AmqpRejectAndDontRequeueException(ex);
        }
    }
}
