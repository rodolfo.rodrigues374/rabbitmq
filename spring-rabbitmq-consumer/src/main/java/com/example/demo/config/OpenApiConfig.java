package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration 
public class OpenApiConfig {
 

	@Bean
	public OpenAPI customOpenAPI(@Value("${application-description}") String appDesciption,
			@Value("${application-version}") String appVersion) {
				
		Contact contact =	new Contact();
		contact.email("rodolfo.rodrigues374@gmail.com");
		contact.setName("Rodolfo Rodrigues");
		contact.setUrl("https://gitlab.com/rodolfo.rodrigues374/rabbitmq");
		

		return new OpenAPI()
						.info(new Info()
						.title("Spring REST API Comsumer Message Rabbitmq")
						.version(appVersion)
						.description(appDesciption)
						.contact(contact)
						.termsOfService("http://swagger.io/terms/")
						.license(new License().name("Apache 2.0").url("http://springdoc.org")));

	}
}
