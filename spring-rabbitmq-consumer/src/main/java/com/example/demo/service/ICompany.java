package com.example.demo.service;

import com.example.demo.dto.CompanyDto;

public interface ICompany {
	
	void receiveMessageComsumer(CompanyDto companyDto);
	
}
