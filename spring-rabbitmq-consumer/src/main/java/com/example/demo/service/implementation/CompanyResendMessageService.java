package com.example.demo.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.amqp.ICompanyResendMessageRabbitMQ;
import com.example.demo.service.ICompanyResendMessageService;

@Service 
public class CompanyResendMessageService implements ICompanyResendMessageService{

	@Autowired
    private ICompanyResendMessageRabbitMQ rePublish;
	
	@Override
	public void resend() {
		 rePublish.resendMessage();
	}

}
