package com.example.demo.service.implementation;

import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.stereotype.Service;

import com.example.demo.dto.CompanyDto;
import com.example.demo.service.ICompany;

@Service
public class CompanyService implements ICompany {

	@Override
	public void receiveMessageComsumer(CompanyDto companyDto) {
		if (companyDto.getCode().equals(50)) {
			throw new AmqpRejectAndDontRequeueException("erro");
		}

		System.out.println(companyDto.toString());

	}

}
