package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.MensagemErroDto;
import com.example.demo.service.ICompanyResendMessageService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/company")
@SuppressWarnings("rawtypes")
@Tag(name = "Company", description = "Company Consumer Message API")
public class CompanyController extends BaseResource {

	@Autowired
	private ICompanyResendMessageService companyResendMessageRabbitMQ;
 
	@Operation(summary = "Resend message company to queue", description = "this operation resends the object Company record to a queue at Rabbitmq.", tags = {
			"company" })
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Compay object resend with to queue successfully.") })
	@RequestMapping(value = "/resend-message-company-to-queue", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity resendMessageCompanyToQueue(HttpServletRequest req) {

		try {
			companyResendMessageRabbitMQ.resend();
			return new ResponseEntity<Void>(HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<MensagemErroDto>(super.logService.log(e, req), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
