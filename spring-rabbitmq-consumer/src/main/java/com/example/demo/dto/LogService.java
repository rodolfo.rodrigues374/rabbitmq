package com.example.demo.dto;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.example.demo.dto.MensagemErroDto;
import com.example.demo.service.ILog;

@Service
public class LogService implements ILog {

	public MensagemErroDto log(final Throwable cause, final HttpServletRequest req) {
		try {
			String mensagem = traceStack(cause).toString() + " - "+ cause.getCause().getMessage().toString();

			return new MensagemErroDto(mensagem, req.getRequestURL().toString());

		} catch (Exception e) {
			return new MensagemErroDto(null, null);
		}
	}

	private StringBuilder traceStack(Throwable cause) {
		StringBuilder log = new StringBuilder();
		if (cause != null) {		
			log.append(cause.getMessage());
		}
		return log;
	}
}
