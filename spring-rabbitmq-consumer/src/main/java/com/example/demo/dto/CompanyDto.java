package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@ToString
public class CompanyDto {
 	
	private Integer code;
	 
	private Integer numberCompany;
	 
	private String nameCompany;
}
